import Vue from 'vue'
import {
  Button,
  Menu,
  MenuItem,
  MenuItemGroup,
  Image
} from 'element-ui'

Vue.use(Button, Menu, MenuItem, MenuItemGroup, Image)
